# OpenVPN in Docker (LEAP Version)

OpenVPN server i a Docker container complete with an EasyRSA PKI CA.

This container is  based on `kylemanna/docker-openvpn`.

The motivation is mostly to easily test the infrastructure for the LEAP VPN
Services with a very easy setup (docker-compose or the likes).

It is not intended for production use, although you're welcome to use it at
your own risk and suggest improvements for your typical workflow.


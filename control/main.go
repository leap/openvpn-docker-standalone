// A simple control plane for the openvpn node
package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"time"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

const (
	CONTROL = "CONTROL_PORT"
)

func main() {
	controlPort := os.Getenv(CONTROL)
	if controlPort == "" {
		fmt.Println(CONTROL, "not set")
		return
	}
	rand.Seed(time.Now().UnixNano())
	log.SetFormatter(&log.TextFormatter{})

	e := echo.New()
	e.File("/ca", "/etc/openvpn/pki/ca.crt")
	e.GET("/cert", cmdGetCert)
	e.Logger.Fatal(e.Start(":" + controlPort))
}

func cmdGetCert(c echo.Context) error {
	clientName := randomString(12)

	cmd := exec.Command("easyrsa", "--batch", "build-client-full", clientName, "nopass")
	err := cmd.Run()
	if err != nil {
		log.Warning(err)
		return echo.NewHTTPError(http.StatusInternalServerError, "could not generate cert")
	}

	var out bytes.Buffer
	cmd = exec.Command("ovpn_getclient", clientName)
	cmd.Stdout = &out

	err = cmd.Run()
	if err != nil {
		log.Warning(err)
		return echo.NewHTTPError(http.StatusInternalServerError, "could not generate cert")
	}
	return c.String(http.StatusOK, out.String())
}

func randomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyz"

	result := make([]byte, length)
	for i := range result {
		result[i] = charset[rand.Intn(len(charset))]
	}
	return string(result)
}
